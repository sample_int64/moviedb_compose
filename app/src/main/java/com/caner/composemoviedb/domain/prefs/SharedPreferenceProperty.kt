package com.caner.composemoviedb.domain.prefs

import android.content.SharedPreferences
import java.lang.ClassCastException
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty
import com.caner.composemoviedb.domain.prefs.getValue as getValue

open class SharedPreferenceProperty<T : Any> (
    defaultValue: T,
    key: String? = null
) : PreferenceProperty<T>(defaultValue, key), ReadWriteProperty<PreferenceStorage, T> {
    override fun getValue(thisRef: PreferenceStorage, property: KProperty<*>): T {
        return thisRef.prefs.value.getValue(getPreferenceKey(property), defaultValue)
    }

    override fun setValue(thisRef: PreferenceStorage, property: KProperty<*>, value: T) {
        thisRef.prefs.value.edit { putValue(key = getPreferenceKey(property), value = value) }

    }

}

inline fun SharedPreferences.edit(action: SharedPreferences.Editor.() -> Unit){
    val editor = edit()
    action(editor)
    editor.apply()
}

@SuppressWarnings("UNCHECKED_CAST")
fun <T : Any> SharedPreferences.getValue(key: String, defaultValue: T): T = when (defaultValue) {
    is Boolean -> getBoolean(key, defaultValue) as T
    is Int -> getInt(key, defaultValue) as T
    is Long -> getLong(key, defaultValue) as T
    is Float -> getFloat(key, defaultValue) as T
    is String -> getString(key, defaultValue) as T
    is Set<*> -> if (defaultValue.all {it is String} ){
        getStringSet(key, defaultValue as Set<String>) as T
    } else {
        throw ClassCastException("Unsupported type")
    }
    else -> throw ClassCastException("Unsupported type")

}

fun <T : Any> SharedPreferences.Editor.putValue(key: String, value: T): SharedPreferences.Editor =
    when (value){
        is Boolean -> putBoolean(key, value)
        is Int -> putInt(key, value)
        is Long -> putLong(key, value)
        is Float -> putFloat(key, value)
        is String -> putString(key, value)
        is Set<*> -> if (value.all {it is String} ){
            putStringSet(key, value as Set<String>)
        } else {
            throw ClassCastException("Unsupported value type")
        }
        else -> throw ClassCastException("Unsupported value type")
    }
