package com.caner.composemoviedb.domain.prefs

import android.content.SharedPreferences

interface PreferenceStorage {
    val prefs: Lazy<SharedPreferences>
}