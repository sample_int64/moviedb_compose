package com.caner.composemoviedb.domain.prefs

import android.content.Context
import android.content.SharedPreferences
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SharedPReferenceStorage @Inject constructor(
    @ApplicationContext private val context: Context, override val prefs: Lazy<SharedPreferences>
) : PreferenceStorage {


}