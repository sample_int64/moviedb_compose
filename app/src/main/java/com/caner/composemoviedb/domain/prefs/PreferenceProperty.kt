package com.caner.composemoviedb.domain.prefs

import kotlin.reflect.KProperty

abstract class PreferenceProperty<T> (
    val defaultValue: T,
    private val preferenceKey: String? = null
){

    private lateinit var preferencePropertyKey: String

    protected fun getPreferenceKey(property: KProperty<*>): String {
        if (!::preferencePropertyKey.isInitialized){
            preferencePropertyKey = preferenceKey ?: property.name.toSnakeCase()
        }

        return preferencePropertyKey
    }

}

fun String.toSnakeCase(divider: String = "_"): String {
    if (isEmpty()) return this

    val builder = StringBuilder()

    forEachIndexed{index, char ->
        if (char.isUpperCase()){
            if (index!=0 && (isLowerCaseElseFalse(index + 1) || isLowerCaseElseFalse(index-1))){
                builder.append(divider)
            }
            builder.append(char)
        } else {
            builder.append(char.uppercaseChar())
        }
    }
    return builder.toString()
}

fun String.isLowerCaseElseFalse(position: Int): Boolean =
    getOrNull(position)?.isLowerCase() ?: false
