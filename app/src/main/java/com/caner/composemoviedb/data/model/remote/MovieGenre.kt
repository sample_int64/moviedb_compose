package com.caner.composemoviedb.data.model.remote

data class MovieGenre(
    val id: Int,
    val name: String
)