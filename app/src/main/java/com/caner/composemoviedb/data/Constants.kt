package com.caner.composemoviedb.data

object Constants {
    const val MOVIE_ID = "movieId"
    const val MOVIE_TYPE = "MOVIE_TYPE"
    const val PAGE = "page"
    const val NOW_PLAYING_MOVIES = 1
    const val UPCOMING_MOVIES = 2
    const val MOVIE_STARTING_PAGE_INDEX = 1

    const val PREF_KEY = "ui_mode_pref"
    const val DARK_MODE = "dark_mode"
}